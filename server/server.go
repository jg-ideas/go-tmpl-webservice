package server

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
	"time"

	"gitlab.com/jg-ideas/go-http-service-common/adapter"
	"golang.org/x/sync/errgroup"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

const defaultRequestTimeout = 2 * time.Second

// OptionFn allows specification of options for creating the server.
type OptionFn func(s *Server)

// Server is an http server
type Server struct {
	port           int
	version        string
	logger         *zap.Logger
	requestTimeout time.Duration
}

// New creates a new *Server
func New(opts ...OptionFn) (*Server, error) {
	s := &Server{
		logger:         zap.NewNop(),
		requestTimeout: defaultRequestTimeout,
	}

	for _, o := range opts {
		o(s)
	}

	if s.port == 0 {
		return nil, errors.New("port is required")
	}

	if s.version == "" {
		return nil, errors.New("version is required")
	}

	return s, nil
}

// RunServer starts the http server
func RunServer(ctx context.Context, srv *Server) error {
	if srv == nil {
		return errors.New("server can't be null")
	}
	mux := mux.NewRouter()
	mux.Handle("/",
		adapter.Apply(
			http.HandlerFunc(srv.Home),
			adapters(srv)...,
		),
	).Methods("GET")

	httpServer := http.Server{
		Handler: mux,
	}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", srv.port))
	if err != nil {
		return fmt.Errorf("failed to listen %w", err)
	}

	eg, ctx := errgroup.WithContext(ctx)
	eg.Go(func() error {
		if err := httpServer.Serve(lis); err != nil {
			return fmt.Errorf("could not start server %w", err)
		}
		return nil
	})

	eg.Go(func() error {
		<-ctx.Done()
		if err := httpServer.Shutdown(ctx); err != nil {
			return fmt.Errorf("could not shutdown server %w", err)
		}
		if err := lis.Close(); err != nil {
			return fmt.Errorf("could not close listener %w", err)
		}
		return nil
	})

	return eg.Wait()
}

func adapters(srv *Server) []adapter.Adapter {
	return []adapter.Adapter{
		adapter.LogAdapter(srv.logger),
		adapter.TimeoutAdapter(srv.requestTimeout),
		adapter.RecoveryAdapter(srv.logger),
	}
}

// WithPort to set the port of the server to run on.
func WithPort(port int) OptionFn {
	return func(s *Server) {
		s.port = port
	}
}

// WithLogger to set the logger.
func WithLogger(logger *zap.Logger) OptionFn {
	return func(s *Server) {
		s.logger = logger
	}
}

// WithVersion to set the version
func WithVersion(version string) OptionFn {
	return func(s *Server) {
		s.version = version
	}
}

// WithRequestTimeout sets a timeout for handling requests, to ensure that they don't hang
func WithRequestTimeout(d time.Duration) OptionFn {
	return func(s *Server) {
		s.requestTimeout = d
	}
}

func (s *Server) json(w http.ResponseWriter, v interface{}, statusCode int) {
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(statusCode)
	err := json.NewEncoder(w).Encode(v)
	if err != nil {
		s.logger.Error(
			"could not encode response",
			zap.String("error.mesage", err.Error()),
		)
	}
}

// nolint:unused
func (s *Server) jsonDecode(body io.ReadCloser, v interface{}) error {
	decoder := json.NewDecoder(body)
	decoder.DisallowUnknownFields()
	return decoder.Decode(v)
}
