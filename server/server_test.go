package server

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
)

func TestNew(t *testing.T) {
	type args struct {
		opts []OptionFn
	}
	tests := []struct {
		name       string
		args       args
		want       *Server
		wantErrStr string
	}{
		{
			name: "should create new server",
			args: args{
				opts: []OptionFn{
					WithPort(1),
					WithVersion("v1"),
				},
			},
			want: &Server{
				logger:         zap.NewNop(),
				requestTimeout: defaultRequestTimeout,
				port:           1,
				version:        "v1",
			},
		},
		{
			name: "port is required",
			args: args{
				opts: []OptionFn{
					WithVersion("v1"),
				},
			},
			wantErrStr: "port is required",
		},
		{
			name: "version is required",
			args: args{
				opts: []OptionFn{
					WithPort(1),
				},
			},
			wantErrStr: "version is required",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := New(tt.args.opts...)

			if tt.wantErrStr != "" {
				require.Error(t, err)
				return
			}

			require.NoError(t, err)
			assert.Equal(t, tt.want, got)
		})
	}
}
