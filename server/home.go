package server

import "net/http"

// Home holds response information for requests to the index page.
type Home struct {
	Status  string `json:"status"`
	Version string `json:"version"`
}

// Home handles requests to the index page.
func (s *Server) Home(w http.ResponseWriter, r *http.Request) {
	s.json(w, &Home{Status: "OK", Version: s.version}, http.StatusOK)
}
