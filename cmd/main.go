package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"gitlab.com/jg-ideas/go-tmpl-webservice/di"
	"gitlab.com/jg-ideas/go-tmpl-webservice/server"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"

	"gitlab.com/jg-ideas/go-tmpl-webservice/config"
)

const (
	exitOK = iota
	exitErr
)

var version string = "v1"
var serviceName string = "go-tmpl-webservice"

func main() {
	env := config.Process()
	container := di.NewContainer(&env)
	logger := container.InjectLogger()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	eg, ctx := errgroup.WithContext(ctx)
	eg.Go(func() error {
		if err := server.RunServer(ctx, container.InjectServer(
			server.WithPort(env.Port),
			server.WithLogger(logger),
			server.WithVersion(version),
			server.WithRequestTimeout(env.Timeout*time.Second),
		)); err != nil {
			logger.Fatal("unable to start server", zap.Error(err))
		}

		return nil
	})

	logger.Info(fmt.Sprintf("%s started", serviceName),
		zap.String("version", version),
		zap.String("env", string(env.Environment)),
		zap.Duration("timeout", env.Timeout),
		zap.Int("port", env.Port),
	)

	if err := eg.Wait(); err != nil {
		logger.Fatal("eg.Wait failed", zap.Error(err))
		os.Exit(exitErr)
	}

	container.Close(ctx)
	os.Exit(exitOK)
}
