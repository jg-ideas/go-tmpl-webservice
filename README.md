## Go Template WebService
Go Template WebService project uses Go's net/http package.

Clone this boilerplate template project that contains common required components to get started writing your own web services in Go.

### How To Use

1. Clone the repo
1. Open a terminal and while inside this repository's root directory, run:
    `rm -rf .git`.
1. Define the `SERVICE_NAME` environment variable.
    `SERVICE_NAME` is the name of your service. This will replace `go-tmpl-webservice` all over the repository.
1. Run `./scripts/new`.
1. (Optional) Edit dev.envrc file.

#### To run
```bash
$ source dev.envrc && go run ./cmd/main.go
```


### How to release
```bash
$ export VERSION=<next version> 
$ make release && git push origin master $VERSION
```