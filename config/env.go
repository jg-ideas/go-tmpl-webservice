package config

import (
	"sync"
	"time"

	"github.com/kelseyhightower/envconfig"
)

type environment string

const (
	// Development environment identifier
	Development environment = "development"
	// Test environment identifier
	Test environment = "test"
	// Production environment identifier
	Production environment = "production"
)

var once sync.Once

// Env used for holding environment variable values
type Env struct {
	Environment environment   `envconfig:"ENVIRONMENT" required:"true"`
	Timeout     time.Duration `envconfig:"TIMEOUT" default:"1m"`
	Port        int           `envconfig:"PORT" required:"true"`
}

// Process to extract values from environment variables to Env
func Process() Env {
	var err error
	var env Env
	once.Do(func() {
		err = envconfig.Process("", &env)
		if err != nil {
			return
		}
	})

	if err != nil {
		panic(err)
	}

	return env
}
