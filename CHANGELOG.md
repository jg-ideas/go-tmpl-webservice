<a name="unreleased"></a>
## [Unreleased]


<a name="v0.0.1"></a>
## v0.0.1 - 2021-08-29
### Added
- changelog
- **all:** template for future projects


[Unreleased]: https://gitlab.com/jg-ideas/go-http-service-common/compare/v0.0.1...HEAD
