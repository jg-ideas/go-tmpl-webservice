package di

import (
	"context"
	"database/sql"

	"gitlab.com/jg-ideas/go-http-service-common/log"
	"gitlab.com/jg-ideas/go-tmpl-webservice/config"
	"gitlab.com/jg-ideas/go-tmpl-webservice/server"
	"go.uber.org/zap"
)

type closeFn func(ctx context.Context)

// Container holds dependencies to be injected into the application.
// Executes cleanup on system exit.
type Container struct {
	env config.Env

	closeFns []closeFn
}

// NewContainer instantiates a container from a given *config.Env
func NewContainer(env *config.Env) *Container {
	if env == nil {
		panic("environment configurations are unset")
	}

	return &Container{
		env: *env,
	}
}

// InjectLogger returns a *zap.Logger configured for production or
// development (e.g. development, test, sandbox) environment.
func (c *Container) InjectLogger() *zap.Logger {
	var logger *zap.Logger
	var err error

	switch c.env.Environment {
	case config.Production:
		logger, err = log.NewProductionLogger()
	default:
		logger, err = log.NewDevelopmentLogger()
	}

	if err != nil {
		panic(err)
	}

	c.closeFns = append(c.closeFns, func(ctx context.Context) {
		_ = logger.Sync()
	})

	return logger
}

func (c *Container) InjectDB() *sql.DB {
	db, err := sql.Open("mysql", "")
	if err != nil {
		panic(err)
	}

	// TODO set the settings

	c.closeFns = append(c.closeFns, func(ctx context.Context) {
		_ = db.Close()
	})

	return db
}

// InjectServer creates a *server.Server from the given options.
func (c *Container) InjectServer(opts ...server.OptionFn) *server.Server {
	srv, err := server.New(opts...)
	if err != nil {
		panic(err)
	}

	return srv
}

// Close will try to close the injected components.
func (c *Container) Close(ctx context.Context) {
	for _, closeFn := range c.closeFns {
		closeFn(ctx)
	}
}
