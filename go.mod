module gitlab.com/jg-ideas/go-tmpl-webservice

go 1.14

require (
	github.com/gorilla/mux v1.8.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/stretchr/testify v1.7.0
	gitlab.com/jg-ideas/go-http-service-common v0.0.2
	go.uber.org/zap v1.16.0
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)
