GOPATH != go env GOPATH

GOPATH_SANITIZED = $(subst \,/,$(GOPATH))

GO_BIN := $(GOPATH_SANITIZED)/bin

all: deps lint test

.PHONY: deps
deps: ## download deps and install tools
	go mod download
	go install github.com/quasilyte/go-consistent@latest
	go install golang.org/x/tools/cmd/goimports@latest
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(GO_BIN)

.PHONY: lint
lint: ## lints the project
	$(GO_BIN)/golangci-lint run $(args) ./...
	$(GO_BIN)/go-consistent $(cons_args) ./...

.PHONY: lint-fix
lint-fix: ## attempts to fix lint errors
	@make lint args='--fix -v' cons_args='-v'

.PHONY: fmt
fmt: ## run goimports on all go files
	@find . -iname "*.go" | xargs $(GO_BIN)/goimports -w

.PHONY: test
test: ## run tests
	go test $(args) -race -cover ./...

.PHONY: test-v
test-v: ## run tests with verbose output
	@make test args='-v'

.PHONY: test-long
test-long: ## run tests 5 times
	go test -v -race -count=5 -coverpkg=./... -coverprofile=coverage.txt ./...

.PHONY: changelog
changelog: ## re-generate the changelog file
	git-chglog --next-tag $(VERSION) --output CHANGELOG.md

.PHONY: release
release: changelog   ## Release a new tag
	@ $(MAKE) --no-print-directory log-$@
	git add CHANGELOG.md
	git commit -m "chore: update changelog for $(VERSION)"
	git tag $(VERSION)
.PHONY: help
help: ## displays this help output
	@awk 'BEGIN {FS = ":.*##"; printf "Usage:\n  make \033[33m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[32m%-25s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

log-%:
	@grep -h -E '^$*:.*?## .*$$' $(MAKEFILE_LIST) | \
		awk \
			'BEGIN { \
				FS = ":.*?## " \
			}; \
			{ \
				printf "\033[36m==> %s\033[0m\n", $$2 \
			}'